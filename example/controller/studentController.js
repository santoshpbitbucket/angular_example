var app = angular.module('myApp', []);

app.controller("studentController", function($scope) {	
	$scope.student = { 
				firstName:'Amit', 
				lastName:'Kumar', 
				rollno:111, 
				subjects:
					[
						{name:'Physics', marks:88},
						{name:'Maths', marks:95}
					],
					
				fullName: function() {
					return $scope.student.firstName + " "+$scope.student.lastName;
				}
			};
});
	
	
	
	