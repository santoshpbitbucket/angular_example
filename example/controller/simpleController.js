var app = angular.module('myApp', []);

app.controller('simpleController', function($scope) {
	$scope.firstName = "Gautam";
	$scope.lastName = "Kumar";
	
	$scope.fullName = function() {
		return $scope.firstName  + " " + $scope.lastName;
	}
});