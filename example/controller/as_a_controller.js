var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope) {
	$scope.value = "Hi from myCtrl";
		
	$scope.show = function() {
		return " show function from the myCtrl ";
	}
});

app.controller('anotherCtrl', function() {
	this.value = "Hi from anotherCtrl";
		
	this.show = function() {
		return " show function from the anotherCtrl ";
	}
});