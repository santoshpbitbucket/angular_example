var app = angular.module('myApp', []);


app.factory('Share' , function() {
	return {orgName:"NIIT"};
})

app.controller('employeeA', function($scope, Share) {
	$scope.firstName = "Ajay";
	$scope.lastName = "Yadav";
	$scope.companyName = "NIIT";
	$scope.sharedName = Share;
	
	$scope.fullName = function() {
		return $scope.firstName  + " " + $scope.lastName;
	}
});

app.controller('employeeB', function($scope, Share) {
	$scope.firstName = "Sumit";
	$scope.lastName = "Gupta";
	$scope.companyName = "NIIT";
	$scope.sharedName = Share;
	
	$scope.fullName = function() {
		return $scope.firstName  + " " + $scope.lastName;
	}
});